<link href="{{asset('css/nav.css')}}" rel="stylesheet" >

<div class="container-fluid col-md-12 nav_bar">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ url('welcome') }}">Book Store</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav nav_item">
                <a class="nav-link " href="#"  role="button"  aria-haspopup="true" aria-expanded="false">
                    Profile
                </a>
                <a class="nav-link " href="{{'login'}}" role="button" aria-haspopup="true" aria-expanded="false">
                    Logout
                </a>
            </ul>
        </div>
    </nav>
</div>
